const express = require('express');
const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;
const password = 'password';

app.get('/encode/:name', (req, res) => {
    const response = Vigenere.Cipher(password).crypt(req.params.name);
    res.end(response);
});

app.get('/decode/:name', (req, res) => {
    const response = Vigenere.Decipher(password).crypt(req.params.name);
    res.end(response);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});